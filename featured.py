from scrapy import Item, Field
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor

# Number of "featured" pages to scrape
# One page = 36 presentations
PAGES_COUNT = 3

class PresentationItem(Item):
  url = Field()
  title = Field()
  views_count = Field()

class FeaturedPresentationsSpider(CrawlSpider):
  name = 'featured'
  allowed_domains = ['slideshare.net']
  start_urls = ['http://www.slideshare.net/featured?foundation_list=ee&format=html&page_offset=%d' % (offset) for offset in range(1, PAGES_COUNT + 1)]
  rules = [Rule(LinkExtractor(restrict_xpaths = '//a[@class="title title-link antialiased"]'), 'parse_presentation_page')]

  def parse_presentation_page(self, r):
    return PresentationItem(
      url = r.url,
      title = r.xpath('//h1/text()').extract()[0],
      views_count = r.xpath('//ul[@class="basicStats"]/li/strong/text()').extract()[0]
    )
